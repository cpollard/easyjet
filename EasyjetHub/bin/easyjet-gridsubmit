#!/bin/env python

import os
import re
import yaml
import argparse
import datetime
import subprocess
from shutil import which
from EasyjetHub.steering.utils.config_files import run_config_arg


def main():
    parser = argparse.ArgumentParser()

    in_list = parser.add_mutually_exclusive_group(required=True)
    in_list.add_argument(
        "--mc-list",
        help="Text file containing MC datasets")
    in_list.add_argument(
        "--data-list",
        help="Text file containing data datasets"
    )

    parser.add_argument(
        "--run-config",
        type=str,
        required=True,
        help="file path to the runConfig",
    )
    parser.add_argument(
        "--exec",
        type=str,
        default="easyjet-ntupler",
        help="The name of the executable",
    )
    parser.add_argument(
        "--campaign",
        default="EJ_%Y_%m_%d_T%H%M%S",
        help=(
            "The name of the campaign. Will be prepended to the output dataset"
            " name. Can include datetime.strftime replacement fields"
        ),
    )
    parser.add_argument(
        "--asetup",
        help="The asetup command, in case it cannot be read from the environment",
    )
    parser.add_argument(
        "--dest-se",
        default=None,
        help="An RSE to duplicate the results to, for example 'DESY-ZN_LOCALGROUPDISK'",
    )
    parser.add_argument(
        "--excluded-site", default=None, help="Any sites to exclude when running"
    )
    parser.add_argument(
        "--nGBperJob", default=None, help="How many GB required per job"
    )
    parser.add_argument(
        "--nFiles", default=None, help="How many files to run on"
    )
    parser.add_argument(
        "--noSubmit",
        action="store_true",
        help="Don't submit the jobs, just create the tarball",
    )
    parser.add_argument(
        "--noTag",
        action="store_true",
        help="Skip git tag creation",
    )

    args = parser.parse_args()

    # check ntupler command is available before submitting
    if not which(args.exec):
        raise RuntimeError(f"Failed to find `{args.exec}`. Check that it is installed")

    # check prun is available before submitting
    if not which("prun"):
        raise RuntimeError("You have not set up panda, run `lsetup panda`")
    
    grid_nickname = check_grid_credentials(return_nickname=True)

    now = datetime.datetime.now()

    # will just return the args.campaign if it doesn't contain any strftime fields (%Y, %m, etc.)
    campaign = now.strftime(args.campaign)

    if not args.noSubmit and not args.noTag:
        create_git_tag(campaign)
        campaign_is_default = args.campaign == parser.get_default("campaign")
        # Avoid creating same dataset names which will cause failing grid jobs
        campaign = campaign if campaign_is_default else f"{campaign}.{now.strftime('%Y_%m_%d_T%H%M%S')}"

    # Prepare some of the prun options here
    submit_opts = {
        "mergeOutput": False,
        "outputs": "TREE:output-tree.root",
        "writeInputToTxt": "IN:in.txt",
        "useAthenaPackages": True,
    }

    if args.noSubmit:
        submit_opts["noSubmit"] = True
    if args.dest_se is not None:
        submit_opts["destSE"] = args.dest_se
    if args.excluded_site is not None:
        submit_opts["excludedSite"] = args.excluded_site
    if args.nGBperJob is not None:
        submit_opts["nGBPerJob"] = args.nGBperJob
    if args.nFiles is not None:
        submit_opts["nFiles"] = args.nFiles

    if args.asetup is None:
        atlas_project = os.environ["AtlasProject"]

        project_dir = os.environ["{0}_DIR".format(atlas_project)]
        project_version = os.environ["{0}_VERSION".format(atlas_project)]

        if project_dir.startswith("/cvmfs/atlas-nightlies.cern.ch"):
            raise ValueError(
                "Cannot deduce the asetup command for a nightly!"
                + " Use the --asetup option"
            )
        submit_opts["athenaTag"] = ",".join([atlas_project, project_version])
    else:
        submit_opts["athenaTag"] = args.asetup

    # copy the runconfig to the submit dir to be available for the exec cmd
    try:
        with open('config.yaml','w') as cfg:
            cfg.write(yaml.dump(run_config_arg(args.run_config)))
    except subprocess.CalledProcessError as e:
        raise OSError(e.returncode, "Failed to copy run config files.")

    proc = subprocess.Popen(
        [
            "prun",
            "--outDS",
            "user.{0}.NONE".format(grid_nickname),
            "--exec",
            "NONE",
            "--noSubmit",
            "--useAthenaPackages",
            "--outTarBall",
            "code.tar.gz",
        ]
    )

    if proc.wait() != 0:
        raise OSError(proc.returncode, "Failed to create tarball")

    submit_opts["inTarBall"] = "code.tar.gz"

    io_list = []
    with open(args.mc_list or args.data_list) as fp:
        ds_list = fp.readlines()
    for ds in ds_list:
        ds = ds.strip()
        if ds.startswith("*"):
            continue
        if not ds:
            continue
        ds_name = ds.rpartition(":")[2]
        ds_name_split = ds_name.split(".")
        ds_tags = ds_name_split[-1]
        if args.mc_list is not None:
            dsid = ds_name_split[1]
        else:
            dsid = f"{ds_name_split[0]}.{ds_name_split[1]}"
        io_list.append(
            {
                "inDS": ds_name,
                "outDS": "user.{0}.{1}.{2}.{3}".format(
                    grid_nickname, campaign, dsid, ds_tags
                ),
            }
        )

    if not io_list:
        raise ValueError("No inputs defined")

    for io in io_list:
        exec_cmd = "{0} %IN -l --run-config {1} --out-file {2}".format(
            args.exec,
            "config.yaml",
            submit_opts["outputs"].removeprefix("TREE:"),
        )

        cmd = [
            "prun",
            "--inDS", io["inDS"],
            "--outDS", io["outDS"],
            "--exec", exec_cmd,
        ]

        for k, v in submit_opts.items():
            if isinstance(v, bool):
                if v:
                    cmd.append(f"--{k}")
            else:
                if isinstance(v, (list, tuple)):
                    v = ",".join(map(str, v))

                cmd += [f"--{k}", v]

        print(" ".join(cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()
        print(out.decode("utf-8"), err.decode("utf-8"))
        if proc.returncode != 0:
            raise OSError(proc.returncode, err)


def check_grid_credentials(return_nickname=False):
    result = subprocess.run(["voms-proxy-info", "--all"], capture_output=True)
    result_output = (result.stdout or result.stderr).decode("utf-8").strip()
    if result.returncode != 0:
        raise RuntimeError(result_output)

    if return_nickname:
        nickname_re = re.search(r'nickname = (\w+)', result_output)
        return nickname_re.group(1) if nickname_re else None


def create_git_tag(tag_name: str, return_tag_exists: bool = False):
    result = subprocess.run(["easyjet-create-git-tag", tag_name], capture_output=True)
    result_message = (result.stderr or result.stdout).decode("utf-8").strip()
    if result.returncode != 0:
        raise RuntimeError(result_message)
    else:
        print(result_message)
        if return_tag_exists:
            return "reusing tag" in result_message.lower()


if __name__ == "__main__":
    main()
